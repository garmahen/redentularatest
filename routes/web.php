<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/catalog', 'CatalogController@index')->name('getCatalog');
Route::get('/catalog/import', 'CatalogController@getImport')->name('getImport');

Route::post('/catalog/parse_xlsx', 'CatalogController@parseXlsx')->name('parseXlsx');
Route::post('/catalog/import_xlsx', 'CatalogController@importXlsx')->name('importXlsx');
