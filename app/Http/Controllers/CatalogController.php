<?php

namespace App\Http\Controllers;

use App\Catalog;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PhpOffice\PhpSpreadsheet\IOFactory;

class CatalogController extends Controller
{

    const FILE_BATCH_SIZE = 1000;

    private $mapping = ['type', 'category', 'manufacturer', 'name', 'code', 'description', 'price', 'guarantee', 'existence'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $catalogs = Catalog::all();
        return view('catalog.index', compact('catalogs'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getImport()
    {
        return view('catalog.import');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function parseXlsx(Request $request)
    {

        ini_set('max_execution_time', 10);
        ini_set('memory_limit', '2024M');

//        $max_size = ini_get('post_max_size');
//        $upload_max = ini_get('upload_max_filesize');

        $success = false;
        $count = 0;
        $tempFileCount = 0;
        $headers = [];
        $fileInfo = [];
        $errors = [];
        $tempDir = time();
        $delimiter = "\t";

        try {

            Validator::make($request->all(), [
                'file' => 'required|max:512000|file|mimetypes:text/csv,text/plain,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel',
            ])->validate();

            $file = $request->file('file');

            $fileName = Carbon::now()->format('YmdHs') . '_' . $file->getClientOriginalName();
            $request->file('file')->storeAs('/catalogs', $fileName);
            $filePath = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'catalogs' . DIRECTORY_SEPARATOR;
            $tmpFilePath = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR;
            $fullFilePath = $filePath . $fileName;
            $catalogsForImport = [];

            if (file_exists($fullFilePath)) {
                $success = true;

                $fileInfo['fileName'] = $file->getClientOriginalName();
                $fileInfo['fileExtension'] = $file->extension();
                $fileInfo['fileType'] = $file->getClientMimeType();
                $fileInfo['fileClientExtension'] = $file->clientExtension();
                $fileInfo['fileStored'] = $fullFilePath;
                $fileInfo['size'] = filesize($fullFilePath);
                $fileInfo['extension'] = $file->getExtension();
                $fileInfo['headersExist'] = (isset($request['header'])) ? true : false;

                $fileExtension = $file->getClientOriginalExtension();

                if ($fileExtension == 'xls' || $fileExtension == 'xlsx') {
                    $spreadsheet = IOFactory::load($fullFilePath);
                    $worksheet = $spreadsheet->getActiveSheet();

                    foreach ($worksheet->getRowIterator() as $row) {
                        $cellIterator = $row->getCellIterator();
                        $cellIterator->setIterateOnlyExistingCells(false);
                        $dataArr = [];
                        foreach ($cellIterator as $cell) {
                            $dataArr[] = trim($cell->getValue());
                        }
                        $catalogsForImport[] = $dataArr;
                    }
                }

                if ($catalogsForImport) {
                    $headers = $catalogsForImport[0];
                    foreach ($headers as $key1 => $header) {
                        if (empty($header)) {
                            unset($headers[$key1]);
                        }
                    }
                }

                $count = count($catalogsForImport);
                mkdir($tmpFilePath . $tempDir, 0777);

                $i = 0;
                $tmpCatalog = [];

                foreach ($catalogsForImport as $catalog) {

                    $tmpCatalog[] = $catalog;

                    if ($i % self::FILE_BATCH_SIZE == 0 && $i > 0) {

                        $fp = fopen($tmpFilePath . $tempDir . DIRECTORY_SEPARATOR . microtime() . '.csv', 'w');

                        foreach ($tmpCatalog as $fields) {
                            fputcsv($fp, $fields, $delimiter);
                        }

                        fclose($fp);
                        $tempFileCount++;
                        $tmpCatalog = [];
                    }

                    $i++;
                }

                $fp = fopen($tmpFilePath . $tempDir . DIRECTORY_SEPARATOR . microtime() . '.csv', 'w');

                foreach ($tmpCatalog as $fields) {
                    fputcsv($fp, $fields, $delimiter);
                }

                fclose($fp);

                $tempFileCount++;
            }
        } catch (\Exception $exception) {
            $success = false;
            $errors[] = $exception->getMessage();
            $errors['line'][] = $exception->getLine();
        }

//        $data = [
//            'success' => $success,
//            'errors' => $errors,
//            'tempDir' => $tempDir,
//            'count' => $count,
//            'headers' => $headers,
//            'fileInfo' => $fileInfo,
//            'delimiter' => $delimiter,
//            'tempFileCount' => $tempFileCount,
//            'listId' => $listId,
//            'mappings' => $this->mapping,
//        ];

        return view('catalog.import_xlsx', [
            'success' => $success,
            'errors' => $errors,
            'tempDir' => $tempDir,
            'count' => $count,
            'headers' => $headers,
            'size' => $fileInfo['size'],
            'mappings' => $this->mapping,
            'headersExist' => $fileInfo['headersExist'],
        ]);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function importXlsx(Request $request)
    {

        $mapping = $request->fields;
        $tempDir = $request->tempDir;


        $pasted = 0;
        $ignored = 0;
        $success = false;
        $errors = [];
        $invalidData = [];
        $file = '';
        $delimiter = "\t";

        $myPath = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . $tempDir;
        $pathForScan = scandir($myPath . DIRECTORY_SEPARATOR);

        if (isset($pathForScan[2])) {
            $pathForScans =  array_slice($pathForScan, 2);

            foreach ($pathForScans as $path){
                $myFile = $path;
                $file = $myPath . DIRECTORY_SEPARATOR . $myFile;

                $count = count($pathForScan);

                if ($count > 1) {
                    $myData = file($file);

                    if (($request->headersExist == 1)) {
                        unset($myData[0]);
                    }

                    $i = 0;
                    foreach ($myData as $catalogInfo) {
                        $catalog = explode($delimiter, $catalogInfo);
                        $catalogData = [];

                        try {

                            $type = '';
                            if (isset($mapping['type'])) {
                                $type = trim($catalog[$mapping['type']]);
                            }

                            $category = '';
                            if (isset($mapping['category'])) {
                                $category = trim($catalog[$mapping['category']]);
                            }

                            $manufacturer = '';
                            if (isset($mapping['manufacturer'])) {
                                $manufacturer = trim($catalog[$mapping['manufacturer']]);
                            }

                            $name = '';
                            if (isset($mapping['name'])) {
                                $name = trim($catalog[$mapping['name']]);
                            }

                            $code = '';
                            if (isset($mapping['code'])) {
                                $code = trim($catalog[$mapping['code']]);
                            }

                            $description = '';
                            if (isset($mapping['description'])) {
                                $description = trim($catalog[$mapping['description']]);
                            }

                            $price = '';
                            if (isset($mapping['price'])) {
                                $price = trim($catalog[$mapping['price']]);
                            }

                            $guarantee = '';
                            if (isset($mapping['guarantee'])) {
                                $guarantee = trim($catalog[$mapping['guarantee']]);
                            }

                            $existence = '';
                            if (isset($mapping['existence'])) {
                                $existence = trim($catalog[$mapping['existence']]);
                            }

//                        if ($type == '' || $category == '' || $manufacturer == '' || $name == '') {
//                            $ignored++;
//                            continue;
//                        }


                            $catalogData = [
                                'type' => $type,
                                'category' => $category,
                                'manufacturer' => $manufacturer,
                                'name' => $name,
                                'code' => $code,
                                'description' => $description,
                                'price' => $price,
                                'guarantee' => $guarantee,
                                'existence' => $existence,
                            ];

                            $validator = Validator::make($catalogData, [
                                'type' => 'required|max:255',
                                'category' => 'required|max:255',
                                'manufacturer' => 'required|max:255',
                                'name' => 'required|max:255',
                                'code' => 'required|unique:catalog,code|max:255',
                                'description' => 'required',
                                'price' => 'required|max:255',
                                'guarantee' => 'required|max:255',
                                'existence' => 'required|max:255',
                            ]);

                            if ($validator->fails()) {
                                $errors[] = $validator->messages();
                                $ignored++;
                            } else {

                                $type = mb_convert_encoding($type, 'UTF-8', 'UTF-8');
                                $category = mb_convert_encoding($category, 'UTF-8', 'UTF-8');
                                $manufacturer = mb_convert_encoding($manufacturer, 'UTF-8', 'UTF-8');
                                $name = mb_convert_encoding($name, 'UTF-8', 'UTF-8');
                                $code = mb_convert_encoding($code, 'UTF-8', 'UTF-8');
                                $description = mb_convert_encoding($description, 'UTF-8', 'UTF-8');
                                $price = mb_convert_encoding($price, 'UTF-8', 'UTF-8');
                                $guarantee = mb_convert_encoding($guarantee, 'UTF-8', 'UTF-8');
                                $existence = mb_convert_encoding($existence, 'UTF-8', 'UTF-8');

                                $catalog = new Catalog([
                                    'type' => $type,
                                    'category' => $category,
                                    'manufacturer' => $manufacturer,
                                    'name' => $name,
                                    'code' => $code,
                                    'description' => $description,
                                    'price' => $price,
                                    'guarantee' => $guarantee,
                                    'existence' => $existence,
                                ]);

                                $catalog->save();
                                $pasted++;
                            }

                        } catch (\Exception $exception) {
                            $invalidData[] = $catalogData;
                            $errors[] = $exception->getMessage();
                            $ignored++;

                        }
                        $i++;
                    }
                    unlink($file);
                }
            }
        }
//        return [
//            'pasted' => $pasted,
//            'ignored' => $ignored,
//            'success' => $success,
//            'errors' => $errors,
//            'invalidData' => $invalidData,
//            'file' => $file,
//        ];

        return view('catalog.import', [
            'pasted' => $pasted,
            'ignored' => $ignored,
            'success' => $success,
            'errors' => $errors,
            'invalidData' => $invalidData,
            'file' => $file,
        ]);
    }
}
