<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'catalog';

    protected $fillable = ['type', 'category', 'manufacturer', 'name', 'code', 'description', 'price', 'guarantee',
        'existence'];
}
