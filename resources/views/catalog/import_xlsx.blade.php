@extends('layouts.app')

@section('title', 'Import XLSX')

@section('content')

    <div class="container">
        <p>Size: {{$size}} bytes</p>
        <p>Count: {{$count}}</p>
    </div>
    <form class="form-horizontal" method="POST" action="{{route('importXlsx')}}">
        <input type="text" name="tempDir" value="{{$tempDir}}" hidden>
        <input type="text" name="headersExist" value="{{$headersExist}}" hidden>
        {{ csrf_field() }}
        <table class="table">
            @foreach ($mappings as $key => $mapping)
                <tr>
                    <td>{{$mapping}}</td>
                    <td>
                        <select name="fields[{{ $mapping }}]">
                            @foreach ($headers as $key2 => $header)
                                <option value="{{ $key2 }}">{{ $header}}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
            @endforeach

        </table>

        <button type="submit" class="btn btn-primary">
            Import Data
        </button>
    </form>
@endsection
