@extends('layouts.app')

@section('title', 'Catalog')

@section('content')
    <table class="table table-bordered mt-2">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Type</th>
            <th scope="col">Category</th>
            <th scope="col">Manufacturer</th>
            <th scope="col">Name</th>
            <th scope="col">Code</th>
            <th scope="col">Description</th>
            <th scope="col">Price</th>
            <th scope="col">Guarantee</th>
            <th scope="col">Existence</th>
        </tr>
        </thead>
        <tbody>
        @foreach($catalogs as $catalog)
            <tr>
                <th scope="row">{{$catalog->id}}</th>
                <td>{{$catalog->type}}</td>
                <td>{{$catalog->category}}</td>
                <td>{{$catalog->manufacturer}}</td>
                <td>{{$catalog->name}}</td>
                <td>{{$catalog->code}}</td>
                <td>{{$catalog->description}}</td>
                <td>{{$catalog->price}}</td>
                <td>{{$catalog->guarantee}}</td>
                <td>{{$catalog->existence}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
