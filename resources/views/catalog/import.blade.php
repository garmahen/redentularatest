@extends('layouts.app')

@section('title', 'Import XLSX')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{route('parseXlsx')}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                                <label for="file" class="col-md-4 control-label">XLSX file to import</label>

                                <div class="col-md-6">
                                    <input id="file" type="file" class="form-control" name="file" required>
                                </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="header" checked> File contains header row?
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Load XLSX
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(isset($pasted))
        <div class="container">
            <p>Pasted: {{$pasted}}</p>
            <p>Ignored: {{$ignored}}</p>
            @if(isset($errors))
                <details>Errors: {{var_dump($errors)}}</details>
            @endif
        </div>
    @endif
@endsection
